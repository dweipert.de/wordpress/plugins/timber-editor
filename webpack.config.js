const CopyWebpackPlugin = require('copy-webpack-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');

module.exports = {
  entry: {},
  output: {
    path: __dirname + '/assets',
  },

  plugins: [
    new CopyWebpackPlugin({
      patterns: [
        { from: __dirname + '/node_modules/codemirror/mode/twig/twig.js', to: 'codemirror/mode/twig/twig.js' },
        { from: __dirname + '/node_modules/codemirror/theme', to: 'codemirror/theme' },
      ],
    }),
  ],

  // minimize copied js and css
  optimization: {
    minimizer: [
      `...`,
      new CssMinimizerPlugin(),
    ],
  },
};
